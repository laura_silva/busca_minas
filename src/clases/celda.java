/*
 *Autores: Laura Daniela Silva Maya -Laura Gabriela Chaves Narváez
 *correo: laurasilva_owo@hotmail.com - laurachaves00@htomail.com
 *Fecha creación: 30/08/2017       
 *Fecha de modificación 30/08/2017
 *Versión 0.0.1
 *Curso Estructura de datos
 *Institución Unidad San Buenaventura
 */
package clases;

import formulario.principal;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.security.Principal;
import javax.swing.JButton;

/**
 *
 * @author usuario
 */
public class celda extends JButton {

    int x;
    int y;
    int tipo_celda;
    boolean visible;
    Color color[];

    public celda(int x, int y) {
        this.x = x;
        this.y = y;
        this.visible = false;
        this.color = new Color[]{Color.BLUE, Color.GREEN, Color.ORANGE, Color.YELLOW, Color.PINK, Color.BLACK};

        this.setMinimumSize(new Dimension(30, 15)); // se establece el tamaño minimo.

        this.setBackground(new java.awt.Color(166, 33, 64));
        this.setFont(new java.awt.Font("Tahoma", 1, 12));
        this.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                celdaActionPerformed(evt);
            }
        });

    }

    public void setTipo_celda(int tipo_celda) {
        this.tipo_celda = tipo_celda;
    }

    public void celdaActionPerformed(java.awt.event.ActionEvent evt) {
        if (!visible && principal.gano == false) {
            this.visible = true;
            this.setBackground(new java.awt.Color(240, 240, 240));
            switch (tipo_celda) {
                case 0:
                    for (int i = 0; i < principal.filas; i++) {
                        for (int j = 0; j < principal.columnas; j++) {
                            if (principal.celda[i][j].getTipo_celda() == 0) {
                                principal.celda[i][j].setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/unnamed.png")));
                            }

                        }
                    }
                    principal.gano = true;
                    break;
                case 1:
                    int cont = 0;
                    this.setBackground(new java.awt.Color(240, 240, 240));
                    for (int i = -1; i <= 1; i++) {
                        if (x + i >= 0 && x + i < principal.filas) {
                            for (int j = -1; j <= 1; j++) {
                                System.out.println(y + i + " " + y + i + " " + principal.columnas);
                                if ((y + j >= 0 && (y + j) < principal.columnas) && principal.celda[x + i][y + j].getTipo_celda() == 0) {
                                    cont++;

                                }
                            }
                        }
                    }
                    this.setText(""+cont);
                    this.setForeground(this.color[cont]);
                    break;
                default:
            }
        }

    }

    public int getTipo_celda() {
        return tipo_celda;
    }

}
